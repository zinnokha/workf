<?php
if (strpos($_SERVER['REQUEST_URI'], 'fbe_dev') !== FALSE){
	$dev = true;
	echo "DEV MODE";
}
else {
	$dev = false;
}
include('process.php');

if (ereg(".*electrovoice.*", strtolower($_SERVER['HTTP_HOST'])) OR ereg(".*electrovoice.*", strtolower($_SERVER['REQUEST_URI']))) {
	$site_domain = 'electrovoice';
	$site_short = 'ev';
}
else if (ereg(".*rtsintercoms.*", strtolower($_SERVER['HTTP_HOST'])) OR ereg(".*rtsintercoms.*", strtolower($_SERVER['REQUEST_URI']))) {
	$site_domain = 'rtsintercoms';
	$site_short = 'rts';
}
else{
	die();
}

define("SITE", $site_domain);
define("SITE_SHORT", $site_short);


?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title></title>
  </head>
  <body>




<style type="text/css">
	.table td, .table th {padding: 3px; border:0; vertical-align: middle;}
	.table tbody tr:hover {background: #d7d7d7;}

	.il {padding: 3px 10px; display: inline-block; font-size: .8rem;}
	.il-single {display: block;}
	.il-dual {width: 49%;}
	.il-dual:nth-of-type(1) {margin-right: 2%;}
	.il-DarkGreen {background: #008800; color: #ffffff;}
	.il-LightGreen {background: #ccffcc}
	.il-Orange {background: #eeaa22}
	.il-Red  {background: #dd0000}

	.legend {background: #efefef; border:1px solid #c1c1c1;  padding: 20px; }
	.legend-item {width: 20px; height: 20px; display: inline-block;}
	.legend p {margin-bottom:0px;}

	.tooltip-inner {
    max-width: 400px !important;
    text-align: left;}

	.product-selector {width: 150px; float: left; border:3px solid #c1c1c1; margin-right: 10px; padding: 10px; text-align: center;}
	.product-selector img {max-height: 90px;}
	.product-selector:hover {border-color: #000000;}
	.product-selector.active {border-color: #007bff;}

	.product {display: none;}
</style>


<div class="container">
	<div class="row">

		<div class="col-12 mb-4">
			<img src="img/<?php echo SITE;?>.jpg">
		</div>
		<div class="col-12">
			<h1 class="mt-3">Frequency Band Estimator</h1>
			<p>Location: <?php echo $request_info['postal'];?> <?php echo $request_info['lat'];?>,<?php echo $request_info['lng'];?>
			| Generated on: <?php echo $request_info['date'];?>
			<br/><a href="https://products.electrovoice.com/fbe/">Generate a new report</a></p>

			<h2 class="mt-4 mb-4">Select a Product</h2>


			<div>
				<div class="" style="display: inline-block;">
					<h5>Wireless Intercom</h5>
						<div class="product-selector" data-for="BTR-800">
							<img src="img/BTR-800-System-2.png">
							<p>BTR-800</p>
						</div>
						<div class="product-selector" data-for="BTR-80N">
							<img src="img/BTR-80N-Family.png">
							<p>BTR-80N</p>
						</div>
						<div class="product-selector" data-for="BTR-30N">
							<img src="img/BTR-30N.jpg">
							<p>BTR-30N</p>
						</div>
				</div>
				<div class="" style="display: inline-block;">
					<h5>Wireless Microphones</h5>
						<div class="product-selector" data-for="R300">
							<img src="img/R300BPFamily.png">
							<p>R300</p>
						</div>
						<div class="product-selector" data-for="RE-3">
							<img src="img/EVRE3Wireless.jpg">
							<p>RE-3</p>
						</div>
				</div>

			</div>

			<?php if($dev){?>
			<div class="product-selector-all">
							DEV Show all products
			</div>
			<? } ?>

		</div>

	</div>



	<div class="row display-product">

		<?php
		// print_r($products_settings);
		foreach ($products as $p){
		?>

		<div id="product-<?php echo $p['name'];?>" class="col-12 mt-5 product">


			<h4 class=""><?php echo $p['name'];?> Recommended bands listed in order of likely effectiveness</h4>
				<?php
				$recommended_bands = array();
				foreach($p['bands'] as $band){

					$recommended_bands[ $band['TX']['name'] ] = 0;
					foreach($band['TX']['channels'] as $channel){ 
						// Tally up the levels.
						$recommended_bands[ $band['TX']['name'] ] += $channel['ch_interference_level'];
					}


				}
				arsort($recommended_bands);
				echo '<ul>';
				foreach ($recommended_bands as $band => $val) {
					if($val > 0) echo '<li><strong>' . $band . '</strong>';
					
					if($dev) echo ' (TX Val:' . $val . ')';

					echo '</li>';
				}
				echo '</ul>';

				?>




			<h4 class="mt-4"><?php echo $p['name'];?> Detailed view</h4>

			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th colspan="3" style="border-bottom: 0px;"></th>
						<th colspan="<?php echo count($p['bands']);?>"class="text-center" style="background: #000000; color:#ffffff; border-bottom: 0px;">Bands</th>
					</tr>

					<tr>
						<th style="width: 100px;">Start Freq</th>
						<th style="width: 100px;">Channel</th>
						<th style="width: 120px;">Highest Signal</th>


						<?php
						$band_order = array();
						foreach($p['bands'] as $band){
						$band_order[] = $band['TX']['name'];

						if(count($band_order) % 2 == 0) $background = '#646464'; 
						else $background = '#555555';

						?>
							
							<th class="text-center" style="background: <?php echo $background;?>; color:#ffffff;"><?php echo $band['TX']['name'];?></th>		

						<?php } // End of bands ?>

					</tr>
				</thead>





				<tbody>
				<?php

				// Sorts the data by channels

		/*
			
			Channel > Band > Type > DATA

		  [21] => Array
		        (
		            [C] => Array
		                (
		                    [TX] => Array
		                        (
		                            [ch_interference_level] => 2
		                            [ch_interference_color] => LightGreen
		                        )

		                )

		            [_channel] => 21
		            [_highest_dbm] => -48.68
		            [_stations] => Array
		                (
		                    [21_K21NU-D] => Array
		                        (
		                            [st_channel] => 21
		                            [st_callsign] => K21NU-D
		                            [st_distance] => 47.7
		                            [st_signal] => -70.569561865894
		                            [st_interference_level] => 3
		                        )

		                    [21_WUMN-LD] => Array
		                        (
		                            [st_channel] => 21
		                            [st_callsign] => WUMN-LD
		                            [st_distance] => 16.68
		                            [st_signal] => -48.676154522834
		                            [st_interference_level] => 2
		                        )

		                )

		        )

		*/

				$channel_data = array();
				foreach($p['bands'] as $band){ 
					foreach($band['TX']['channels'] as $channel){ 
						$channel_data [$channel['channel']] [$band['TX']['name']] ['TX'] ['ch_interference_level'] = $channel['ch_interference_level'];
						$channel_data [$channel['channel']] [$band['TX']['name']] ['TX'] ['ch_interference_color'] = $channel['ch_interference_color'];

						// Extra stuff

						// Find the highest dbm
						if(count($channel['stations']) > 0){
							$highest_dbm = -1000;
							foreach($channel['stations'] as $st){
								if($st['st_signal'] > $highest_dbm) $highest_dbm = round($st['st_signal'],0) . ' dBm';
							}
						}
						else $highest_dbm = '--';

						$channel_data [$channel['channel']] ['_channel'] = $channel['channel'];
						$channel_data [$channel['channel']] ['_highest_dbm'] = $highest_dbm;
						$channel_data [$channel['channel']] ['_stations'] = $channel['stations'];

					} // End of channels

					if(isset($band['RX'])){
						foreach($band['RX']['channels'] as $channel){ 
							$channel_data [$channel['channel']] [$band['RX']['name']] ['RX'] ['ch_interference_level'] = $channel['ch_interference_level'];
							$channel_data [$channel['channel']] [$band['RX']['name']] ['RX'] ['ch_interference_color'] = $channel['ch_interference_color'];

						// Extra stuff

						// Find the highest dbm
						if(count($channel['stations']) > 0){
							$highest_dbm = -1000;
							foreach($channel['stations'] as $st){
								if($st['st_signal'] > $highest_dbm) $highest_dbm = round($st['st_signal'],0) . ' dBm';
							}
						}
						else $highest_dbm = '--';

						$channel_data [$channel['channel']] ['_channel'] = $channel['channel'];
						$channel_data [$channel['channel']] ['_highest_dbm'] = $highest_dbm;
						$channel_data [$channel['channel']] ['_stations'] = $channel['stations'];

						} // End of channels
					}

				} // End of bands
				ksort($channel_data);

				// echo '<pre>'; print_r($channel_data);
				

					foreach($channel_data as $ch){
					?>&nbsp;
					<tr>
						<td><?php echo ch_to_freq($ch['_channel'])[0]?></td>
						<td><span class="channel" data-toggle="tooltip" data-html="true"  data-placement="bottom" title="<div class='station-info'>Stations:<?php
							foreach ($ch['_stations'] as $d){
								echo '<br/>' . $d['st_callsign'] . ' &nbsp;&nbsp;&nbsp;&nbsp; ' . $d['st_distance'] . ' mi &nbsp;&nbsp;&nbsp;&nbsp; ' . round($d['st_signal'],2) . ' dBm';;
							}
						 ?></div>"><?php echo $ch['_channel']?></span></td>
						<td><?php echo $ch['_highest_dbm']?></td>
						<?php
						foreach($band_order as $band){

							echo '<td class="text-center">';

							// If this band is found, lets output some data!
							if(isset($ch[$band])){
								
								// If there are ever 2 items per
								if(count($ch[$band]) == 1) $il_class = 'il-single';
								if(count($ch[$band]) == 2) $il_class = 'il-dual';

								if(isset($ch[$band]['TX'])){
									echo '<span class="il il-'.$ch[$band]['TX']['ch_interference_color'].' '.$il_class.'"> TX </span>';
								}

								if(isset($ch[$band]['RX'])){
									echo '<span class="il il-'.$ch[$band]['RX']['ch_interference_color'].' '.$il_class.'"> RX </span>';
								}

							}

							// No band is found, lets skip it!
							else{
								echo '';
							}

							echo '</td>';

						}
						?>

					</tr>
					<?php
					}
					?>

				</tbody>

			</table>

		</div><!-- End of col-12 -->



		<?
		} // End of product

		?>

	</div>


	<div class="row justify-content-center">
		<div class="col-sm-12 col-md-8 mt-5">

			<div class="legend">
				<h5>Legend</h5>
				<p class="mb-2">Interference is calculated based on the station type, location, and signal strength.
				<p><span class="legend-item il-Red"></span> is strong interference. Do not use any channels with strong interference.</p>
				<p><span class="legend-item il-Orange"></span> is medium interference. Try to stay away from medium if possible.</p>
				<p><span class="legend-item il-LightGreen"></span> is weak interference. Use channel if needed.</p>
				<p><span class="legend-item il-DarkGreen"></span> is an open channel. These are the channels available to use.</p>
			</div>

		</div>
	</div>

</div>

<?php if($dev){?>
<div class="container-fluid" >
<div class="row my-5">
	<div class="col-12 text-center">
		<div id="station-data-toggle">DEV Show all station data</div>
	</div>
	<div id="station-data" class="col-12" style="display: none;">
		<h2>Station data</h2>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th style="border-bottom: 0px;">Channel</th>
					<th style="border-bottom: 0px;">Callsign</th>
					<th style="border-bottom: 0px;">Service</th>
					<th style="border-bottom: 0px;">DOM</th>
					<th style="border-bottom: 0px;">City State</th>
					<th style="border-bottom: 0px;">Distance</th>
					<th style="border-bottom: 0px;">Base ERP</th>
					<th style="border-bottom: 0px;">Calc ERP</th>
					<th style="border-bottom: 0px;">Base Signal</th>
					<th style="border-bottom: 0px;">Calc Signal</th>
					<th style="border-bottom: 0px;">Dir Ant</th>
					<th style="border-bottom: 0px;">Ant Pat Logic</th>
					<th style="border-bottom: 0px;">Radio Hor Logic</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach($stations as $st){ ?>
					<tr>
						<td><?php echo $st['info']['c_station_channel'];?></td>
						<td><?php echo $st['info']['c_callsign'];?></td>
						<td><?php echo $st['info']['c_service'];?></td>
						<td><?php echo $st['info']['c_dom_status'];?></td>
						<td><?php echo $st['location']['station_city'];?> <?php echo $st['location']['station_state'];?></td>
						<td><?php echo $st['location']['distance'];?> <?php echo $st['location']['bearing_dir_compass'];?></td>
						<td><?php echo round($st['signal_calc']['station_erp_base'],2);?></td>
						<td><?php echo round($st['signal_calc']['station_erp_updated'],2);?></td>
						<td><?php echo round($st['signal_calc']['signal_base'],2);?></td>
						<td><?php echo round($st['signal_calc']['signal_final'],2);?></td>
						<td><?php echo $st['signal_calc']['ant_diretional'];?></td>
						<td><?php echo $st['signal_calc']['ant_pat_logic'];?></td>
						<td><?php echo $st['signal_calc']['radio_hor_logic'];?></td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
</div>
</div>
<?php } ?>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

<?php
if($dev) { ?>
<script>
	function ga_vcollect(p1, p2, p3, p4, p5, p6){ga(p1, p2, p3, p4, p5);}
	function ga(p1, p2, p3, p4, p5){console.log('GA Request: ' + p1 + ' | ' + p2 + ' | ' + p3 + ' | ' + p4 + ' | ' + p5);};
</script>
<?php } else { ?>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-6534508-5','auto');ga('send','pageview');

	function ga_vcollect(p1, p2, p3, p4, p5, p6){
		$.ajax({
			method: "POST",
			url: "/vcollect/",
			data:{u:"<?php echo $_SERVER['REQUEST_URI'];?>",t:p2,c:p3,a:p4,l:p5,ej:p6}
		});
		ga(p1, p2, p3, p4, p5);
	}
</script>
<?php } ?>

<script type="text/javascript">

	ga_vcollect('send', 'event', 'FBE', 'View', '<?php echo filter_input(INPUT_GET,'q',FILTER_SANITIZE_STRING);?>');


	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});


	$('.product-selector').click(function(){

		ga_vcollect('send', 'event', 'FBE', 'Products', $(this).attr('data-for') );

		target_id = '#product-' + $(this).attr('data-for');
		$('.product').hide();
		$('.product-selector').removeClass('active');
		$(target_id).show();
		$(this).addClass('active');
	});


	// Dev stuff
	$('.product-selector-all').click(function(){
		$('.product').show();
		$('.product-selector').addClass('active');
	});

	$('#station-data-toggle').click(function(){
		$('#station-data-toggle').hide();
		$('#station-data').show();
	});




</script>

  </body>
</html>