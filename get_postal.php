<?php 

if(isset($_GET['p'])) {

	$postal = filter_input(INPUT_GET,'p',FILTER_SANITIZE_NUMBER_INT);;

	include_once('functions.php');
	include_once('../../global/connect.php');

	$postal_query = $connection->prepare('SELECT zip,state,latitude,longitude FROM audioID.zip_codes WHERE `zip` = "'.$postal.'" LIMIT 1');
	$postal_query->execute();
	$results = $postal_query->fetchAll(\PDO::FETCH_OBJ);

	if(count($results) > 0){

		$lat_dms = DDtoDMS($results[0]->latitude);
		$lng_dms = DDtoDMS($results[0]->longitude);

		$data['status'] = 'success';
		$data['date'] = date('Y-m-d H:i:s');
		$data['lat'] = $results[0]->latitude;
		$data['lng'] = $results[0]->longitude;
		$data['postal'] = $results[0]->zip;
		$data['region'] = $results[0]->state;



	}
	else{
		$data['status'] = 'error';
		$data['message'] = 'No postal found';

	}

}
else {
	$data['status'] = 'error';
	$data['message'] = 'No postal defined';
}

echo json_encode($data);

?>

