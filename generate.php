<?php 


if(isset($_GET['lat']) && isset($_GET['lng'])) {

	$lat =  filter_input(INPUT_GET,'lat',FILTER_SANITIZE_STRING);
	$lng = filter_input(INPUT_GET,'lng',FILTER_SANITIZE_STRING);

	include_once('functions.php');
	include_once('../../global/connect.php');

	if($lat !== NULL){
	
		include_once('query_fcc.php');
		
		$data['status'] = 'success';
		$data['lat'] = $lat;
		$data['lng'] = $lng;
		$data['lat_dms'] = $lat_dms;
		$data['lng_dms'] = $lng_dms;
	}
	else{
		$data['status'] = 'error';
		$data['message'] = '';
	}

}
else {
	$data['status'] = 'error';
	$data['message'] = 'No coords defined';
}


if(isset($_GET['debug'])){
	echo '<pre>';
	print_r($data);
?>

&lt;fbe_request&gt;
date = <?php echo date('Y-m-d H:i:s');?>;
request_url = <?php echo $url;?>;
lat = <?php echo $lat;?>;
lng = <?php echo $lng;?>;
execution_time = 0;
&lt;/fbe_request&gt;

<?php
}



echo json_encode($data);

?>