<?php

// echo $uniq_id = md5('20210315' * rand());die();

$start_time = microtime(true); 

$lat_dms = DDtoDMS($lat);
$lng_dms = DDtoDMS($lng);

// create curl resource
$ch = curl_init();

$url = 'https://transition.fcc.gov/fcc-bin/tvq?dist=120&dlat2='.$lat_dms['deg'].'&mlat2='.$lat_dms['min'].'&slat2='.$lat_dms['sec'].'&dlon2='.$lng_dms['deg'].'&mlon2='.$lng_dms['min'].'&slon2='.$lng_dms['sec'].'&size=9&list=2';
// $url = 'https://products.electrovoice.com/fbe_dev/dev_source.html';

// set url
curl_setopt($ch, CURLOPT_URL, $url );

//return the transfer as a string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// $output contains the output string
$curl_content = curl_exec($ch);

$httpcode = curl_getinfo($ch);

// echo $curl_content; die();

curl_close($ch);  
// close curl resource to free up system resources

$end_time = microtime(true);
$execution_time = ($end_time - $start_time); 

$content = "<fbe_request>
date = ".date('Y-m-d H:i:s').";
request_url = ".$url.";
lat = ".$lat.";
lng = ".$lng.";
execution_time = ".$execution_time.";
</fbe_request>
" . $curl_content;


// echo $content; die();

$uniq_id = md5(date('YmdHis') * rand());
$id = strtoupper( substr($uniq_id,5,12) );
$fileloc = "tmp/" . $id . ".txt";

$fp = fopen( $fileloc,"wb");
fwrite($fp,$content);
fclose($fp);

$data['url'] = $url;
$data['time'] = $execution_time;
$data['id'] = $id;



?>