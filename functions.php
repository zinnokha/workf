<?php


function DMStoDD($deg,$min,$sec, $dir = NULL)
{


	// Converting DMS ( Degrees / minutes / seconds ) to decimal format
	$decimal = $deg+((($min*60)+($sec))/3600);

	//reverse for south or west coordinates; north is assumed
	$dir = strtolower($dir);
	if($dir == 's' || $dir == 'w') {
		$decimal *= -1;
	}

	return $decimal;
}    

function DDtoDMS($dec)
{
		// Converts decimal format to DMS ( Degrees / minutes / seconds ) 
		$vars = explode(".",$dec);
		$deg = $vars[0];
		$tempma = "0.".$vars[1];

		$tempma = $tempma * 3600;
		$min = floor($tempma / 60);
		$sec = $tempma - ($min*60);

		return array("deg"=>$deg,"min"=>$min,"sec"=>$sec);
}    

/*

https://www.dougv.com/2009/07/calculating-the-bearing-and-compass-rose-direction-between-two-latitude-longitude-coordinates-in-php/

*/

function getRhumbLineBearing($lat1, $lon1, $lat2, $lon2) {
	//difference in longitudinal coordinates
	$dLon = deg2rad($lon2) - deg2rad($lon1);
 
	//difference in the phi of latitudinal coordinates
	$dPhi = log(tan(deg2rad($lat2) / 2 + pi() / 4) / tan(deg2rad($lat1) / 2 + pi() / 4));
 
	//we need to recalculate $dLon if it is greater than pi
	if(abs($dLon) > pi()) {
		if($dLon > 0) {
			$dLon = (2 * pi() - $dLon) * -1;
		}
		else {
			$dLon = 2 * pi() + $dLon;
		}
	}
	//return the angle, normalized
	return (rad2deg(atan2($dLon, $dPhi)) + 360) % 360;
}

function getCompassDirection($bearing) {
	$tmp = round($bearing / 22.5);
	switch($tmp) {
		case 1:
			$direction = "NNE";
			break;
		case 2:
			$direction = "NE";
			break;
		case 3:
			$direction = "ENE";
			break;
		case 4:
			$direction = "E";
			break;
		case 5:
			$direction = "ESE";
			break;
		case 6:
			$direction = "SE";
			break;
		case 7:
			$direction = "SSE";
			break;
		case 8:
			$direction = "S";
			break;
		case 9:
			$direction = "SSW";
			break;
		case 10:
			$direction = "SW";
			break;
		case 11:
			$direction = "WSW";
			break;
		case 12:
			$direction = "W";
			break;
		case 13:
			$direction = "WNW";
			break;
		case 14:
			$direction = "NW";
			break;
		case 15:
			$direction = "NNW";
			break;
		default:
			$direction = "N";
	}
	return $direction;
}

/*
function getCompass360($bearing) {
	$tmp = round($bearing / 22.5);
	switch($tmp) {
		case 1:
			$direction = "20";
			break;
		case 2:
			$direction = "45"; // Actaully 45
			break;
		case 3:
			$direction = "70";
			break;
		case 4:
			$direction = "90";
			break;
		case 5:
			$direction = "110";
			break;
		case 6:
			$direction = "140";
			break;
		case 7:
			$direction = "160";
			break;
		case 8:
			$direction = "180";
			break;
		case 9:
			$direction = "215"; // Actaully 215
			break;
		case 10:
			$direction = "225"; // Actaully 225
			break;
		case 11:
			$direction = "250";
			break;
		case 12:
			$direction = "270";
			break;
		case 13:
			$direction = "290";
			break;
		case 14:
			$direction = "310";
			break;
		case 15:
			$direction = "320";
			break;
		default:
			$direction = "0";
	}
	return $direction;
}*/


function round360($val) {
	return round($val,-1);
}


// echo getCompassDirection(getRhumbLineBearing(44.732569, -93.28909, 44.995583333333, -93.44905555555));
// die();



/*

https://stackoverflow.com/questions/10053358/measuring-the-distance-between-two-coordinates-in-php

*/

function haversineGreatCircleDistance(
  $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 3959)
{
  // convert from degrees to radians
  $latFrom = deg2rad($latitudeFrom);
  $lonFrom = deg2rad($longitudeFrom);
  $latTo = deg2rad($latitudeTo);
  $lonTo = deg2rad($longitudeTo);

  $latDelta = $latTo - $latFrom;
  $lonDelta = $lonTo - $lonFrom;

  $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
	 cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
  return round($angle * $earthRadius,2);
}



function ch_to_freq($ch){

	$channels = array(
		2	=>	array(54,60),
		3	=>	array(60,66),
		4	=>	array(66,72),
		5	=>	array(76,82),
		6	=>	array(82,88),
		7  => array(174,180),
		8  => array(180,186),
		9  => array(186,192),
		10 => array(192,198),
		11 => array(198,204),
		12 => array(204,210),
		13 => array(210,216),
		14 => array(470,476),
		15 => array(476,482),
		16 => array(482,488),
		17 => array(488,494),
		18 => array(494,500),
		19 => array(500,506),
		20 => array(506,512),
		21 => array(512,518),
		22 => array(518,524),
		23 => array(524,530),
		24 => array(530,536),
		25 => array(536,542),
		26 => array(542,548),
		27 => array(548,554),
		28 => array(554,560),
		29 => array(560,566),
		30 => array(566,572),
		31 => array(572,578),
		32 => array(578,584),
		33 => array(584,590),
		34 => array(590,596),
		35 => array(596,602),
		36 => array(602,608),
		37 => array(608,614),
		38 => array(614,620),
		39 => array(620,626),
		40 => array(626,632),
		41 => array(632,638),
		42 => array(638,644),
		43 => array(644,650),
		44 => array(650,656),
		45 => array(656,662),
		46 => array(662,668),
		47 => array(668,674),
		48 => array(674,680),
		49 => array(680,686),
		50 => array(686,692),
		51 => array(692,698),
	);

	if(array_key_exists($ch, $channels)){
		return $channels[$ch];
	}
	else{
		echo "Error: No valid channel.!";
		die();
	}
}

function freq_to_ch($freq){

	$channels = array(
		2	=>	array(54,60),
		3	=>	array(60,66),
		4	=>	array(66,72),
		5	=>	array(76,82),
		6	=>	array(82,88),
		7  => array(174,180),
		8  => array(180,186),
		9  => array(186,192),
		10 => array(192,198),
		11 => array(198,204),
		12 => array(204,210),
		13 => array(210,216),
		14 => array(470,476),
		15 => array(476,482),
		16 => array(482,488),
		17 => array(488,494),
		18 => array(494,500),
		19 => array(500,506),
		20 => array(506,512),
		21 => array(512,518),
		22 => array(518,524),
		23 => array(524,530),
		24 => array(530,536),
		25 => array(536,542),
		26 => array(542,548),
		27 => array(548,554),
		28 => array(554,560),
		29 => array(560,566),
		30 => array(566,572),
		31 => array(572,578),
		32 => array(578,584),
		33 => array(584,590),
		34 => array(590,596),
		35 => array(596,602),
		36 => array(602,608),
		37 => array(608,614),
		38 => array(614,620),
		39 => array(620,626),
		40 => array(626,632),
		41 => array(632,638),
		42 => array(638,644),
		43 => array(644,650),
		44 => array(650,656),
		45 => array(656,662),
		46 => array(662,668),
		47 => array(668,674),
		48 => array(674,680),
		49 => array(680,686),
		50 => array(686,692),
		51 => array(692,698),
	);

	foreach($channels as $ch => $val){
		if($freq >= $val[0] AND $freq <= $val[1]){
			$data = $ch;
		}
	}

	if($data){
		return $data;
	}
	else{
		echo "Error: No valid freq.!";
		die();
	}
}


?>