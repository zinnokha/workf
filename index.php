<?php
if (strpos($_SERVER['REQUEST_URI'], 'fbe_dev') !== FALSE){
	$dev = true;
	echo "DEV MODE";
}
else {
	$dev = false;
}


if (ereg(".*electrovoice.*", strtolower($_SERVER['HTTP_HOST'])) OR ereg(".*electrovoice.*", strtolower($_SERVER['REQUEST_URI']))) {
	$site_domain = 'electrovoice';
	$site_short = 'ev';
}
else if (ereg(".*rtsintercoms.*", strtolower($_SERVER['HTTP_HOST'])) OR ereg(".*rtsintercoms.*", strtolower($_SERVER['REQUEST_URI']))) {
	$site_domain = 'rtsintercoms';
	$site_short = 'rts';
}
else{
	die();
}

define("SITE", $site_domain);
define("SITE_SHORT", $site_short);


?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title></title>
  </head>
  <body>

<style type="text/css">
	#output {background: #000000; color: #ffffff; padding: 20px 10px; font-family: Courier New;}
	#output p {margin: 0;}

	#switch-to-coords, #switch-to-postal {text-decoration: underline; font-size: .9rem;}
</style>



<div class="container">
	<div class="row">
		
		<div class="col-12 mb-4 ">
			<img src="img/<?php echo SITE;?>.jpg">
		</div>

		<div class="col-12 mb-5" style="min-height: 450px;">
			<h1 class="mt-3 mb-3">Frequency Band Estimator</h1>

			<p><strong>This tool is only for the United States!</strong></p>
			<p>This tool estimates the interference from TV channels that you might experience in your area while using a wireless system. Based on estimates it will recommend the most compatible bands for your wireless system.</p>


			<div id="action">
				<div id="postal">
					<h4 class="mt-4 mb-3">Search by Postal</h4>
					<form id="postal-form">

						<input type="text" name="p" placeholder="Postal">
						<input type="submit">
					</form>

					<p><span id="switch-to-coords">Switch to latitude & longitude input</span></p>
				</div>

				<div id="coords" style="display: none;">
					<h4 class="mt-4 mb-3">Search by Coordinates </h4>
					<form id="coords-form">

						<input type="text" name="lat" placeholder="Latitude">
						<input type="text" name="lng" placeholder="Longitude">
						<input type="submit">
					</form>

					<p><span id="switch-to-postal">Switch to postal input</span></p>

				</div>

			</div>

			<div id="start-report" style="display: none;">
				
				<div class="mt-4">
					<h4 class="mt-b mb-3">Generating your report</h4>
					<p>Thank you for using our FBE. Please sit tight while we process your report, this should take about 40-60 seconds.</p>
					<!-- <p><strong>Time elapsed: 0s</strong></p> -->
				</div>

				<div id="result" style="display: none;" class="my-3"></div>

				<div id="output" style="display: none; min-height: 190px;">
					<p>(1/6) Starting...</p>
					<p id="loading-1" style="display: none;">(2/6) Querying the FCC database for all  stations in your area.</p>
					<p id="loading-2" style="display: none;">(3/6) Calculating each station's antenna strength and direction.</p>
					<p id="loading-3" style="display: none;">(4/6) Calculating each station's data in relevance to your location.</p>
					<p id="loading-4" style="display: none;">(5/6) Generating a report.</p>
					<p id="loading-complete" style="display: none;" class="m-0">(6/6) Complete. Please wait while we redirect you...</p>
				</div>

			</div>


	</div>
</div>






<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>


<?php
if($dev) { ?>
<script>
	function ga_vcollect(p1, p2, p3, p4, p5, p6){ga(p1, p2, p3, p4, p5);}
	function ga(p1, p2, p3, p4, p5){console.log('GA Request: ' + p1 + ' | ' + p2 + ' | ' + p3 + ' | ' + p4 + ' | ' + p5);};
</script>
<?php } else { ?>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-6534508-5','auto');ga('send','pageview');

	function ga_vcollect(p1, p2, p3, p4, p5, p6){
		$.ajax({
			method: "POST",
			url: "/vcollect/",
			data:{u:"<?php echo $_SERVER['REQUEST_URI'];?>",t:p2,c:p3,a:p4,l:p5,ej:p6}
		});
		ga(p1, p2, p3, p4, p5);
	}
</script>
<?php } ?>

<script>
console.log('Loaded');

$('#switch-to-coords').click(function(){
	$('#postal').fadeOut();
	$('#coords').delay(500).fadeIn();
});

$('#switch-to-postal').click(function(){
	$('#coords').fadeOut();
	$('#postal').delay(500).fadeIn();});

$('#postal-form').submit(function(){
	thisForm = $(this);

	$.ajax({
		url: "get_postal.php",
		method: "GET",
		data: thisForm.serialize(),
		dataType: "json",
		success:function(data){
			if(data.status == 'success'){
				$('input[name="lat"]').val(data.lat);
				$('input[name="lng"]').val(data.lng);
				$('#coords-form').submit();
			}
			else{
				alert('Error. Invalid postal. Try again.');
			} 
		},
		error:function(data) {
			alert('Error.');
		}
	});

return false;
});


$('#coords-form').submit(function(){
	thisForm = $(this);

	// Do validation
	// https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyA0dpcpjq65gIcgivvf8C25jUG3QqWTfhY

	if( $('input[name="lat"]').val() != '' && $('input[name="lng"]').val() != '' ){

		$('#action').fadeOut();
		$('#start-report').delay(500).fadeIn();
		$('#output').delay(3000).fadeIn();
		$('#loading-1').delay(10000).fadeIn();
		$('#loading-2').delay(18000).fadeIn();
		$('#loading-3').delay(25000).fadeIn();
		$('#loading-4').delay(30000).fadeIn();

		$.ajax({
			url: "generate.php",
			method: "GET",
			data: thisForm.serialize(),
			dataType: "json",
			success:function(data){
				if(data.status == 'success'){
					// query_fcc(data.lat_dms,data.lng_dms);
					$('#loading-complete').fadeIn();

					link = 'https://products.electrovoice.com/fbe/view.php?q='+data.id;

					$('#result').html('Report link: <a href="'+link+'">'+link+'</a>')
					$('#result').delay(1500).slideDown();

					setTimeout(function () {
					window.location.href = link; 
					}, 6000); 

				}
				else{
					$('#start-report').remove();
					alert('Error. Please refresh this page and try again.');

				} 
			},
			error:function(data) {
				$('#start-report').remove();
				alert('Error. Please refresh this page and try again.');
			}
		});


	}
	else{
		alert('Error. Invalid coordinates. Try again.');
	}

return false;
});












</script>

  </body>
</html>