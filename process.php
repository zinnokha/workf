<?php
/*

https://products.electrovoice.com/fbe_new_dev/rev7/get_fcc.php?lat=44.732561&lng=-93.28909&debug

https://transition.fcc.gov/fcc-bin/tvq?dist=120&dlat2=44&mlat2=43&slat2=57&dlon2=-93&mlon2=17&slon2=20&size=9&list=2

Process Flow

	1	Submits Request location info
		-> Background -- Javascript AJAX query to get.php that saved this source file
		https://transition.fcc.gov/fcc-bin/tvq?dist=120&dlat2=44&mlat2=43&slat2=57&dlon2=-93&mlon2=17&slon2=20&size=9&list=2
		-> Returns hash that links to that saved source file
		-> Javascript to redirect that page to view.php?q=HASHID

	2	The View page loads and process all the data and spits everything out uing PHP. Simple.


*/



/*

	Error checking of the file

*/


if( !isset($_GET['q'])){
	echo "Invalid file"; die();
}
$url = 'tmp/' . filter_input(INPUT_GET,'q',FILTER_SANITIZE_STRING) . '.txt';





if(isset($_GET['debug'])){
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	echo "<pre>";
}

include_once('functions.php');
include_once('products.php');
include_once('../../global/connect.php');


// $url = 'https://transition.fcc.gov/fcc-bin/tvq?dist=120&dlat2=44&mlat2=43&slat2=57&dlon2=-93&mlon2=17&slon2=20&size=9&list=2';

$html = file_get_contents($url);
$dom = new DOMDocument();

// Disable DOM errors https://stackoverflow.com/questions/7082401/avoid-domdocument-xml-warnings-in-php
$previous_value = libxml_use_internal_errors(TRUE);

$dom->loadHTML($html);




/*

	Checks and sets the FBE settings

*/

$request_results = $dom->getElementsByTagName('fbe_request');
foreach ($request_results as $result) {
	foreach(explode("\n", $result->textContent) as $row){
		$row =  str_replace(";", "", $row);
		$row_items = explode(" = ", $row);
		if(in_array($row_items[0], array('date','request_url','lat','lng','region','postal'))){
			$request_info[$row_items[0]] = $row_items[1];
		}
	}
}

// Throw an error if we can't find these items
if( !isset($request_info['date']) && !isset($request_info['request_url']) && !isset($request_info['lat']) && !isset($request_info['lng']) ){
	echo "Invalid Data"; die();
}

// $lat = $_GET['lat'];
// $lng = $_GET['lng'];

$lat = $request_info['lat'];
$lng = $request_info['lng'];




/*

	Grabs each station info

*/

$fcc_results = $dom->getElementsByTagName('script');

$row_count = 0;

foreach ($fcc_results as $result) {
	
	$s = array();
	$st_arr = array();

	if (substr($result->textContent, 0, 12) === "<!-- \n  omit"){

		$result_cleaned = $result->textContent;

		// Clean out the values
		$result_cleaned = substr($result_cleaned ,8) ;						// Removes the begenning chars: <!-- \n  
		$result_cleaned = substr($result_cleaned, 0, -4);					// Removes the ending chars: \n -->
		$result_cleaned = str_replace("= '", "= ", $result_cleaned);		// Removes both ' and  " chars
		$result_cleaned = str_replace("= \"", "= ", $result_cleaned);
		$result_cleaned = str_replace("';", "", $result_cleaned);			// Removes both ' and  " chars and semicolin
		$result_cleaned = str_replace("\";", "", $result_cleaned);
		$result_cleaned = str_replace(";", "", $result_cleaned);
		$result_cleaned = str_replace("\n  ", "\n", $result_cleaned);		// Removes the spaces
		$result_cleaned = str_replace("\n ", "\n", $result_cleaned);

		// echo $result_cleaned;die();

		// Break down the rows
		$rows = explode("\n", $result_cleaned);

		foreach ($rows as $row){
			$row_items = explode(" = ",  $row);

			if(isset($row_items[1]))
				$s[ $row_items[0] ] = $row_items[1];
			else
				$s[ $row_items[0] ] = NULL;

		}

				$st_arr['info']['facility_id'] = $s['facility_id'];
				$st_arr['info']['c_callsign'] = $s['c_callsign'];
				$st_arr['info']['c_service'] = $s['c_service'];
				$st_arr['info']['c_dom_status'] = $s['c_dom_status'];
				$st_arr['info']['c_callsign'] = $s['c_callsign'];
				$st_arr['info']['c_station_channel'] = $s['c_station_channel'];
				$st_arr['info']['p_erp_max'] = $s['p_erp_max'];
				$st_arr['info']['c_da_ind'] = $s['c_da_ind'];



		// Find lat & lon in deg
		$st_lat = DMStoDD($s['c_lat_deg'],$s['c_lat_min'],$s['c_lat_sec'],$s['c_lat_dir']);
		$st_lng = DMStoDD($s['c_lon_deg'],$s['c_lon_min'],$s['c_lon_sec'],$s['c_lon_dir']);



		// Find distance from starting point
		$distance = haversineGreatCircleDistance($st_lat, $st_lng, $lat, $lng);

				$st_arr['location']['user_coord'] = $lat .','. $lng; // Comment this out when prod
				$st_arr['location']['station_coord'] = $st_lat .','. $st_lng;
				$st_arr['location']['station_city'] = $s['c_comm_city_app'];
				$st_arr['location']['station_state'] = $s['c_comm_state_app'];
				$st_arr['location']['distance'] = $distance;



		// Find the bearings
		$st_line_bearing = getRhumbLineBearing($st_lat, $st_lng, $lat, $lng);
		
		$bearing_dir_compass = getCompassDirection($st_line_bearing);
		$bearing_dir_360 = $st_line_bearing;

				$st_arr['location']['bearing_dir_compass'] = $bearing_dir_compass; 
				#$st_arr['location']['bearing_dir_360'] = $bearing_dir_360; 
				$st_arr['location']['bearing_dir_360'] = $bearing_dir_360; 


		/*

			Start ERP logic

		*/

		// Populates some general information
		$freqs = ch_to_freq($s['c_station_channel']);

		$start_freq = $freqs[0];
		$end_freq = $freqs[1];
		$mid_freq = $start_freq + (($end_freq - $start_freq) / 2);
		$station_erp = $s['p_erp_max'];

				$st_arr['signal_calc']['start_freq'] = $start_freq;
				$st_arr['signal_calc']['end_freq'] = $end_freq;
				$st_arr['signal_calc']['mid_freq'] = $mid_freq;
				$st_arr['signal_calc']['distance'] = $distance;
				$st_arr['signal_calc']['station_erp_base'] = $station_erp;



		/*

			Antenna direction & pattern

		*/

		$facility_db_data_results = NULL;
		$facility_db_query_method = NULL;
		$ant_rotation = NULL;
		$bearing_dir_360_adjusted = NULL;
		$ant_pat_logic = NULL;
		$ant_pat_value = NULL;

		if($s[ 'c_da_ind' ]  == 'Y'){

			// Search for the antenna id
			$facility_db_data_query = $connection->prepare('
			SELECT antenna_id, ant_rotation FROM comm_fbe.tv_eng_table
			WHERE facility_id = '.$s[ 'facility_id' ].' AND station_channel = '.$s[ 'c_station_channel' ].' AND effective_erp = '.$s[ 'p_erp_max' ].' AND tv_dom_status = "'.$s[ 'c_dom_status' ].'" AND antenna_id <> 0
			ORDER BY `id` DESC');
			$facility_db_data_query->execute();
			$facility_db_data_results = $facility_db_data_query->fetchAll(\PDO::FETCH_OBJ);
			$facility_db_query_method = 'QUERY_1';


			// If no results, Search for the antenna id again without the tv_dom_status
			if(count($facility_db_data_results) == 0){
				$facility_db_data_query = $connection->prepare('
				SELECT antenna_id, ant_rotation FROM comm_fbe.tv_eng_table
				WHERE facility_id = '.$s[ 'facility_id' ].' AND station_channel = '.$s[ 'c_station_channel' ].' AND effective_erp = '.$s[ 'p_erp_max' ].' AND antenna_id <> 0
				ORDER BY `id` DESC');
				$facility_db_data_query->execute();
				$facility_db_data_results = $facility_db_data_query->fetchAll(\PDO::FETCH_OBJ);
				$facility_db_query_method = 'QUERY_2';
			}



			if(count($facility_db_data_results) > 0){

				// Antenna rotartion lofgic
				$ant_rotation = $facility_db_data_results[0]->ant_rotation;

				$bearing_dir_360_adjusted = ($bearing_dir_360 + 360) - $facility_db_data_results[0]->ant_rotation;

				if($bearing_dir_360_adjusted >= 360){
					$bearing_dir_360_adjusted = $bearing_dir_360_adjusted - 360;
				}

				$bearing_dir_360_adjusted = round360($bearing_dir_360_adjusted);

				$st_arr['signal_calc']['ant_rotation'] = $ant_rotation;
				$st_arr['signal_calc']['bearing_dir_360_adjusted'] = $bearing_dir_360_adjusted;



				$antenna_pat_query = $connection->prepare('
				SELECT field_value FROM comm_fbe.ant_pattern
				WHERE antenna_id = '.$facility_db_data_results[0]->antenna_id.'
				AND azimuth = '.$bearing_dir_360_adjusted.'
				LIMIT 1');
				$antenna_pat_query->execute();
				$antenna_pat_results = $antenna_pat_query->fetchAll(\PDO::FETCH_OBJ);

				if(count($antenna_pat_results) > 0){
					$ant_pat_logic = 'Y';
					$ant_pat_value = $antenna_pat_results[0]->field_value;


					// Recalc the ERP
					$station_erp = $ant_pat_value * $station_erp;
				}
				else{
					$ant_pat_logic = 'NO_ANT_PAT_FOUND';
					$ant_rotation = NULL;
					$bearing_dir_360_adjusted = NULL;
					$ant_pat_value = NULL;
				}

			}
			else{
				$ant_pat_logic = 'NO_ANT_ID_FOUND';
				$ant_pat_value = NULL;
			}


			$st_arr['ant_id'] = json_encode($facility_db_data_results);
			$st_arr['facility_db_query'] = 'facility_id = '.$s[ 'facility_id' ].' AND station_channel = '.$s[ 'c_station_channel' ].' AND effective_erp = '.$s[ 'p_erp_max' ].' AND tv_dom_status = "'.$s[ 'c_dom_status' ].'" AND antenna_id <> 0';


		}
		else{
			$ant_pat_logic = 'N';
			$ant_pat_value = NULL;
		}


				$st_arr['signal_calc']['ant_diretional'] = $s[ 'c_da_ind' ];
				$st_arr['signal_calc']['facility_db_query_method'] = $facility_db_query_method;
				$st_arr['signal_calc']['ant_pat_logic'] = $ant_pat_logic;

				$st_arr['signal_calc']['ant_pat_value'] = $ant_pat_value;
				$st_arr['signal_calc']['station_erp_updated'] = $station_erp;




		/*

			Antenna Height
			
		*/

		$antenna_height_results = NULL;
	 	$ant_height_method = NULL;
	 	$ant_height = NULL;

		// Get Antenne height HAAT OVER ASML
		if($s['p_haat_max'] > 1){
		 	$ant_height_method = 'p_haat_max__HAAT';
		 	$ant_height = $s['p_haat_max'];
		}
		else{

			// Search for AGL value
			$antenna_height_query = $connection->prepare('
			SELECT hag_rc_mtr FROM comm_fbe.tv_eng_table
			WHERE facility_id = '.$s[ 'facility_id' ].' AND station_channel = '.$s[ 'c_station_channel' ].' AND effective_erp = '.$s[ 'p_erp_max' ].' AND tv_dom_status = "'.$s[ 'c_dom_status' ].'"
			ORDER BY `id` DESC');
			$antenna_height_query->execute();
			$antenna_height_results = $antenna_height_query->fetchAll(\PDO::FETCH_OBJ);
			$ant_height_method = 'hag_rc_mrt_db__AGL_QUERY_1';

			// If no results, Search for the AGL value again without the tv_dom_status
			if(count($antenna_height_results) == 0){
				$antenna_height_query = $connection->prepare('
				SELECT hag_rc_mtr FROM comm_fbe.tv_eng_table
				WHERE facility_id = '.$s[ 'facility_id' ].' AND station_channel = '.$s[ 'c_station_channel' ].' AND effective_erp = '.$s[ 'p_erp_max' ].' 
				ORDER BY `id` DESC');
				$antenna_height_query->execute();
				$antenna_height_results = $antenna_height_query->fetchAll(\PDO::FETCH_OBJ);
				$ant_height_method = 'hag_rc_mrt_db__AGL_QUERY_2';

			}

			if(count($antenna_height_results) > 0 && $antenna_height_results[0]->hag_rc_mtr != 0){
				$ant_height = $antenna_height_results[0]->hag_rc_mtr;
			}
			else{
			 	$ant_height_method = 'p_rcamsl_maxx__AMSL';
				$ant_height = $s['p_rcamsl_max'];
			}

		}

		$st_arr['signal_calc']['ant_height_method'] = $ant_height_method;
		$st_arr['signal_calc']['ant_height'] = $ant_height;



		// Finding the unaltered Signal at Zip.
		$station_erp_dbm = 60 + 10 * log10($station_erp); // dbm
		$path_loss_db = 36.6 + 20 * log10($distance) + 20 * log10($mid_freq); // db
		$signal_base = $station_erp_dbm - $path_loss_db; // dbm

		$st_arr['signal_calc']['signal_base'] = $signal_base; 


		// Radio Hor
		$radio_hor = (4.12*sqrt($ant_height))/1.6;

		// If within radio hor use unaltered Signal at Zip.
		if($distance <= $radio_hor){
			$radio_hor_logic = 'N';
			$attenuation_diff = NULL;
			$signal_radio_hor = $signal_base; 
		}
		// Outside of radio hor, do special stuff!
		else{
			$radio_hor_logic = 'Y';
			$attenuation_diff = $distance - $radio_hor;
			$attenuation_db = (1 * $attenuation_diff) * -1;
			$free_space = $attenuation_db + $signal_base;
			$signal_radio_hor = $free_space;
		}

		$st_arr['signal_calc']['radio_hor'] = $radio_hor;
		$st_arr['signal_calc']['radio_hor_logic'] = $radio_hor_logic;
		$st_arr['signal_calc']['attenuation_diff'] = $attenuation_diff; // Comment this out when prod
		$st_arr['signal_calc']['signal_radio_hor'] = $signal_radio_hor;

		$st_arr['signal_calc']['signal_final'] = $signal_radio_hor;




		/*

			End of ERP logic

		*/


		$stations[$s['c_station_channel'] . '_' . $s['c_callsign']] = $st_arr;
		// $stations[$row] = $s;
		$row_count ++;
	}

}



		/*

			Start of Products Logic

		*/

foreach($products_settings as $product){

	// p_arr = product data
	$p_arr = array();
	$p_arr['name'] = $product['name'];
	
	foreach($product['bands'] as $band){

		$band_name = $band['name'];
		$band_type = $band['type'];

		$p_arr ['bands'] [$band_name] [$band_type]  ['name'] = $band_name;
		$p_arr ['bands'] [$band_name] [$band_type] ['freq_start'] = $band['freq_start'];
		$p_arr ['bands'] [$band_name] [$band_type] ['freq_end'] = $band['freq_end'];

		$freq_start_ch = freq_to_ch($band['freq_start']);
		$freq_end_ch = freq_to_ch($band['freq_end']);

		$p_arr ['bands'] [$band_name] [$band_type] ['freq_start_ch'] = $freq_start_ch;
		$p_arr ['bands'] [$band_name] [$band_type] ['freq_end_ch'] = $freq_end_ch;

		// Go though each channel
		for($ch = $freq_start_ch; $ch < $freq_end_ch; $ch++)
		{

			$ch_arr = array();

			$ch_arr ['channel'] = $ch; 
			$ch_arr ['stations'] = array();
			$ch_interference_data = array();

			foreach($stations as $station){

				$st_identifier = $station['info']['c_station_channel'].'_'.$station['info']['c_callsign'];

				if($ch == $station['info']['c_station_channel']){
				
					if($station['signal_calc']['signal_final'] <= -50){
						$st_interference_level = 3;
					}
					else if($station['signal_calc']['signal_final'] <= -40){
						$st_interference_level = 2;
					}
					else if($station['signal_calc']['signal_final'] <= -35){
						$st_interference_level = 1;
					}
					else{
						$st_interference_level = 0;
					}

					$ch_arr ['stations'] [$st_identifier] ['st_channel'] = $station['info']['c_station_channel'];
					$ch_arr ['stations'] [$st_identifier] ['st_callsign'] = $station['info']['c_callsign'];
					$ch_arr ['stations'] [$st_identifier] ['st_distance'] = $station['location']['distance'];
					$ch_arr ['stations'] [$st_identifier] ['st_signal'] = $station['signal_calc']['signal_final'];
					$ch_arr ['stations'] [$st_identifier] ['st_interference_level'] = $st_interference_level;

					// Adds this to the parent array for later processing
					$ch_interference_data[] = $st_interference_level;

				}

			} // End of stations


			/*
				
				Does the final interferenee check for this channel
		
			*/


			if(in_array(0, $ch_interference_data)){
				$ch_interference_level = 0;
				$ch_interference_color = 'Red';
			}
			else if(in_array(1, $ch_interference_data)){
				$ch_interference_level = 1;
				$ch_interference_color = 'Orange';
			}
			else if(in_array(2, $ch_interference_data)){
				$ch_interference_level = 2;
				$ch_interference_color = 'LightGreen';
			}
			else{
				$ch_interference_level = 3;
				$ch_interference_color = 'DarkGreen';
			}

			$ch_arr ['ch_interference_data'] = $ch_interference_data; 
			$ch_arr ['ch_interference_level'] = $ch_interference_level; 
			$ch_arr ['ch_interference_color'] = $ch_interference_color; 

			$p_arr ['bands'] [$band_name] [$band_type] ['channels'] [$ch] = $ch_arr;
		}

		$products[$product['name']] = $p_arr;
		
	} // End of bands
} // End of product settings



if(isset($_GET['debug'])){
	echo  $url;
	//  print_r($products_settings);
	echo '<h1>Request info</h1>' ;
	print_r($request_info);
	echo '<h1>Product & Bands</h1>' ;
	print_r($products);
	echo '<h1>Station Data</h1>' ;
	print_r($stations);
}

?>