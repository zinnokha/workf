<?php

		$products_settings = array(

			array(
				'name' => 'R300',
				'bands' => array(
					array(
						'name' => 'C',
						'type' => 'TX',
						'freq_start' => 516,
						// 'freq_end' => 532,
						'freq_end' => 536, // Added to include channel 24
					),
				),
			),

			array(
				'name' => 'RE-3',
				'bands' => array(
					array(
						'name' => '5L',
						'type' => 'TX',
						'freq_start' => 488,
						'freq_end' => 524,
					),
					array(
						'name' => '5H',
						'type' => 'TX',
						'freq_start' => 560,
						'freq_end' => 596,
					),
					array(
						'name' => '6M',
						'type' => 'TX',
						'freq_start' => 653,
						// 'freq_end' => 663,
						'freq_end' => 668,
					),
				),
			),



			array(
				'name' => 'BTR-800',
				'bands' => array(


					array(
						'name' => 'A3',
						'type' => 'TX',
						'freq_start' => 518,
						'freq_end' => 536,
					),
					array(
						'name' => 'A3',
						'type' => 'RX',
						'freq_start' => 653,
						'freq_end' => 663,
						// 'freq_end' => 663,
						'freq_end' => 668, // Added to include channel 46
					),
					array(
						'name' => 'B3',
						'type' => 'TX',
						'freq_start' => 536,
						'freq_end' => 554,
					),
					array(
						'name' => 'B3',
						'type' => 'RX',
						'freq_start' => 653,
						'freq_end' => 663,
						// 'freq_end' => 663,
						'freq_end' => 668, // Added to include channel 46
					),
					array(
						'name' => 'C3',
						'type' => 'TX',
						'freq_start' => 554,
						'freq_end' => 572,
					),
					array(
						'name' => 'C3',
						'type' => 'RX',
						'freq_start' => 653,
						// 'freq_end' => 663,
						'freq_end' => 668, // Added to include channel 46

					),
					array(
						'name' => 'E88',
						'type' => 'TX',
						'freq_start' => 590,
						'freq_end' => 608,
					),
					array(
						'name' => 'E88',
						'type' => 'RX',
						'freq_start' => 470,
						'freq_end' => 488,
					),
					array(
						'name' => 'F3',
						'type' => 'TX',
						'freq_start' => 482,
						'freq_end' => 500,
					),
					array(
						'name' => 'F3',
						'type' => 'RX',
						'freq_start' => 653,
						// 'freq_end' => 663,
						'freq_end' => 668, // Added to include channel 46
					),
					array(
						'name' => 'H3',
						'type' => 'TX',
						'freq_start' => 500,
						'freq_end' => 518,
					),
					array(
						'name' => 'H3',
						'type' => 'RX',
						'freq_start' => 653,
						// 'freq_end' => 663,
						'freq_end' => 668, // Added to include channel 46
					),



					array(
						'name' => 'FD',
						'type' => 'TX',
						'freq_start' => 482,
						'freq_end' => 500,
					),
					array(
						'name' => 'FD',
						'type' => 'RX',
						'freq_start' => 572,
						'freq_end' => 590, 
					),

					array(
						'name' => 'FE',
						'type' => 'TX',
						'freq_start' => 482,
						'freq_end' => 500,
					),
					array(
						'name' => 'FE',
						'type' => 'RX',
						'freq_start' => 590,
						'freq_end' => 608, 
					),

					array(
						'name' => 'HE',
						'type' => 'TX',
						'freq_start' => 500,
						'freq_end' => 518,
					),
					array(
						'name' => 'HE',
						'type' => 'RX',
						'freq_start' => 590,
						'freq_end' => 608, 
					),

				),
			),

			array(
				'name' => 'BTR-80N',
				'bands' => array(

					array(
						'name' => 'A3',
						'type' => 'TX',
						'freq_start' => 518,
						'freq_end' => 536,
					),
					array(
						'name' => 'A3',
						'type' => 'RX',
						'freq_start' => 653,
						// 'freq_end' => 663,
						'freq_end' => 668, // Added to include channel 46
					),
					array(
						'name' => 'B3',
						'type' => 'TX',
						'freq_start' => 536,
						'freq_end' => 554,
					),
					array(
						'name' => 'B3',
						'type' => 'RX',
						'freq_start' => 653,
						// 'freq_end' => 663,
						'freq_end' => 668, // Added to include channel 46
					),
					array(
						'name' => 'C3',
						'type' => 'TX',
						'freq_start' => 554,
						'freq_end' => 572,
					),
					array(
						'name' => 'C3',
						'type' => 'RX',
						'freq_start' => 653,
						// 'freq_end' => 663,
						'freq_end' => 668, // Added to include channel 46
					),
					array(
						'name' => 'F3',
						'type' => 'TX',
						'freq_start' => 482,
						'freq_end' => 500,
					),
					array(
						'name' => 'F3',
						'type' => 'RX',
						'freq_start' => 653,
						// 'freq_end' => 663,
						'freq_end' => 668, // Added to include channel 46
					),
					array(
						'name' => 'H3',
						'type' => 'TX',
						'freq_start' => 500,
						'freq_end' => 518,
					),
					array(
						'name' => 'H3',
						'type' => 'RX',
						'freq_start' => 653,
						// 'freq_end' => 663,
						'freq_end' => 668, // Added to include channel 46
					),


										array(
						'name' => 'FD',
						'type' => 'TX',
						'freq_start' => 482,
						'freq_end' => 500,
					),
					array(
						'name' => 'FD',
						'type' => 'RX',
						'freq_start' => 572,
						'freq_end' => 590, 
					),

					array(
						'name' => 'FE',
						'type' => 'TX',
						'freq_start' => 482,
						'freq_end' => 500,
					),
					array(
						'name' => 'FE',
						'type' => 'RX',
						'freq_start' => 590,
						'freq_end' => 608, 
					),

					array(
						'name' => 'HE',
						'type' => 'TX',
						'freq_start' => 500,
						'freq_end' => 518,
					),
					array(
						'name' => 'HE',
						'type' => 'RX',
						'freq_start' => 590,
						'freq_end' => 608, 
					),
				),
			),



			array(
				'name' => 'BTR-30N',
				'bands' => array(
					array(
						'name' => 'F10',
						'type' => 'TX',
						'freq_start' => 482 ,
						'freq_end' => 500, 
					),
					array(
						'name' => 'F10',
						'type' => 'RX',
						'freq_start' => 174,
						'freq_end' => 198, 
					),
					array(
						'name' => 'F13',
						'type' => 'TX',
						'freq_start' => 482,
						'freq_end' => 500, 
					),
					array(
						'name' => 'F13',
						'type' => 'RX',
						'freq_start' => 192,
						// 'freq_end' => 216, 
						'freq_end' => 470, // Added to include channel 13
					),

					array(
						'name' => 'H10',
						'type' => 'TX',
						'freq_start' => 500 ,
						'freq_end' => 518, 
					),
					array(
						'name' => 'H10',
						'type' => 'RX',
						'freq_start' => 174,
						'freq_end' => 198, 
					),
					array(
						'name' => 'H13',
						'type' => 'TX',
						'freq_start' => 500,
						'freq_end' => 518, 
					),
					array(
						'name' => 'H13',
						'type' => 'RX',
						'freq_start' => 192,
						// 'freq_end' => 216, 
						'freq_end' => 470, // Added to include channel 13
					),

					array(
						'name' => 'A10',
						'type' => 'TX',
						'freq_start' => 518 ,
						'freq_end' => 536, 
					),
					array(
						'name' => 'A10',
						'type' => 'RX',
						'freq_start' => 174,
						'freq_end' => 198, 
					),
					array(
						'name' => 'A13',
						'type' => 'TX',
						'freq_start' => 518,
						'freq_end' => 536, 
					),
					array(
						'name' => 'A13',
						'type' => 'RX',
						'freq_start' => 192,
						// 'freq_end' => 216, 
						'freq_end' => 470, // Added to include channel 13
					),

					array(
						'name' => 'B10',
						'type' => 'TX',
						'freq_start' => 536 ,
						'freq_end' => 554, 
					),
					array(
						'name' => 'B10',
						'type' => 'RX',
						'freq_start' => 174,
						'freq_end' => 198, 
					),
					array(
						'name' => 'B13',
						'type' => 'TX',
						'freq_start' => 536,
						'freq_end' => 554, 
					),
					array(
						'name' => 'B13',
						'type' => 'RX',
						'freq_start' => 192,
						// 'freq_end' => 216, 
						'freq_end' => 470, // Added to include channel 13
					),

					array(
						'name' => 'C10',
						'type' => 'TX',
						'freq_start' => 554 ,
						'freq_end' => 572, 
					),
					array(
						'name' => 'C10',
						'type' => 'RX',
						'freq_start' => 174,
						'freq_end' => 198, 
					),
					array(
						'name' => 'C13',
						'type' => 'TX',
						'freq_start' => 554,
						'freq_end' => 572, 
					),
					array(
						'name' => 'C13',
						'type' => 'RX',
						'freq_start' => 192,
						// 'freq_end' => 216, 
						'freq_end' => 470, // Added to include channel 13
					),


				),
			),


		);

?>